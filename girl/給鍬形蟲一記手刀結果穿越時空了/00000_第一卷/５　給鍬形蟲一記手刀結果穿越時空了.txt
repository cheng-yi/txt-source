無機質的天空顏色
在視野中擴展的未來都市

悠哉的鍬形虫爬在肩上

這裡是哪裡呢

我只是和我的親親寵物鍬形虫
一起玩鬧而已啊這怎麼回事

陷入恐慌的我
開始不斷對鍬形虫使出手刀

一邊叫著「快回去！快回去！」

一邊在路上攻擊昆虫的我
被留著好像現代插花髮型的警官

說了「那邊那個牙齒長得像沉降海岸的你！」

我被未來人指出自卑點了

小心我對你祖先亂搞喔

平成原人眼眶帶淚的哭訴

「我只是用手刀劈鍬形虫而已！」
「是十年前絕種的鍬形虫！」

警官浮現出驚愕的表情
照相信我的警官的說法來看

這裡是五十年後的世界

只要見到這個時代的你
那大概就能知道回去的方法了吧

他死巴著不放查出了我家
去拜訪一下結果那邊住著我的孫女

牙齒排列很明顯的遺傳過去了
都經過兩個世代了還是像到很該死

平成原人眼眶帶淚的激勵

「以港口來說非常的優秀啦！」
「這個時代的你在這裡」

被帶到的地方是醫院

自被宣告餘命一個月那時算起
今天剛好滿一個月

老人的臉瘦骨嶙峋
即使如此還是明白是自己

未來的自己就像是
在等待這刻般的開口

「你什麼都不必說
你想說的我全都知道
如果現在告訴你一切的話
那今日死去的命運應該能夠改變吧
但我要說的只有一件事」

「今後你將會無數次的
無數次無數次的後悔
無數次無數次的受傷
無數次無數次的哭泣吧

不過每一次的經驗
在你隨著時光流逝細細領會後

終有一天會漸入佳境
讓你捨不得放手

所以你就一無所知的回去吧

我過得很幸福」

我對失去溫度的老人
流下的淚水

碰到鍬形虫的那瞬間
眨眼間就被熟悉的風景所包圍

天空顏色仍是湛藍
天空顏色仍是湛藍
天空顏色仍是湛藍
天空顏色仍是湛藍