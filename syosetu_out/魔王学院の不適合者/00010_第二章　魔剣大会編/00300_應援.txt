第二天早上──

回了一次家後我又來到了魔法醫院羅格諾斯。

「嗯⋯⋯⋯⋯」

昏倒的米莎醒了過來
她呆呆的看著我。

「⋯⋯阿諾斯大人⋯⋯雷呢⋯⋯？」
「去德爾佐格爾了」
「那麼⋯⋯已經是早上了嗎⋯⋯」

米莎朝著希拉的方向看去。
雖然病情比來這裡的時候稍微穩定一些，但是還不能大意。
我以為能在天亮前能用〈根源變換〉恢復到正常水準，真沒辦法。
米莎和希拉的力量波長差太多了。啊，效率太差的話，彌撒的身體也會吃不消。
如果沒有蕾阻止，我就會阻止她。

「到決賽結束為止，你就給我老實的呆著」

我用〈創造建築〉的魔法做了一個可以放在指尖的小玻璃球。

「發生什麼事的話就打碎那個玻璃球。可以使影縫無效化」

使用〈光源〉的魔法，附加條件為〈條件〉玻璃球破裂時發動。〈光源〉是產生光的魔法。如果從所有的角度用光照射完全消去影子，影縫的短劍就不能發揮效果。

「再見」
「那個⋯⋯阿諾斯大人⋯⋯！」

米莎叫住了我。

「怎麼了？」

問了一下，她用認真的目光看著我。

「我能再用〈根源變換〉試試嗎？」
「你要做什麼？」
「在決賽開始前就讓雷的母親復原。那樣的話，之後就只要阿諾斯大人從雷體內拔出契約的魔劍就可以了」
「以〈根源變換〉的效率，來不及的」

想讓希拉恢復到能走的程度至少需要十天
而且，如果繼續使用這種力量的話，這次米莎也會不妙。

「儘管如此，總比什麼都不做強」
「就算向神祈禱也不會發生奇跡」
「⋯⋯可能是這樣吧。但是，即使沒有奇跡發生，也不能放棄啊」

米莎露出了殷切的表情。

「我不想後悔。以後再做，那個時候再做就好了什麼的，我已經不想這麼想了。即使什麼都做不了，我也要盡我所能。」

呼姆。也不是沒有理解現狀嗎。

「我了解你的覺悟了」

我用〈根源變換〉的魔法，再次連接起米莎和希拉的根源。

「如果奇跡發生的話，就帶著希拉來戰鬥場吧。給雷戴上項圈的傢伙們也會注意到的吧，之後我會想辦法解決的」

米莎點了點頭。

「我明白了」
「再見」

使用〈轉移〉，我來到了德爾佐格德魔王學院。
一邊走向闘技場的休息室，一邊思考著決賽的事情。
雖然那樣對米莎說了，但希拉大概來不及了吧。
即使期待奇跡什麼的，也沒有辦法啊。
雷被告知要在決賽時對我做點什麼。
但是，那傢伙不擅長魔法。
不能使用預備的魔劍。
雷的武器是能斬裂魔法式的伊尼迪奧。
雖然它很強大，但相對的能做的事就非常少
那麼阿沃斯・迪爾海維亞在計劃著什麼？
嘛，不管在計劃些什麼都無所謂
關鍵是治療希拉的精靈病，拔掉雷體內的契約的魔劍，打回去
就可以了。
沒什麼大不了的。
進入了了休息室。
雷早就在對面的休息室待機了吧。
要說的話，我不想被捲入這種無聊的謀略中，想沒有顧慮地試著戰鬥。
在盯著金剛鐵劍發獃的同時，等待著決賽的開始。
聽到了咚咚敲門的聲音。

「誰啊？」

過了一會才有回應。

「⋯⋯我⋯⋯」

是米莎的聲音。

「怎麼了？」

嘩啦一聲把門拉開一條縫，從那裡偷偷地窺視著米莎的臉。

「助威」
「我嗎？」

米莎點了點頭。

「這樣啊。話說回來，為什麼只露出臉呢？」
「可以進來嗎？」
「當然可以」

於是，完全打開們，米莎進來了。

「你在緊張嗎？」
「緊張？呼姆。嘛，真想體驗一回啊。真不巧，我還沒有經驗」

米莎噼噼啪啪地眨了兩下眼。

「怎麼了？」
「很有阿諾斯的風格」

米莎笑著說道。

「不和莎夏一起嗎」
「在阿諾斯媽媽那裡」
「嚯。稀奇的事也是有啊」

如果是米莎的話，因為在學做料理所以能理解，但是莎夏並不是那麼和媽媽關係好。

「聽說昨天被襲擊了」
「母桑からか？」

米莎點了點頭。

「我會保護她的，所以阿諾斯就專心於決賽。莎夏的留言」

她可真是個小機靈鬼。

「闘技場上有沒有什麼變化？」

米莎歪著腦袋。

「有誰悄悄潛入的痕跡什麼的」
「沒有變化」

呼姆。轉生前的愛米莉亞的屍體放著不管了，不過，好像在觀眾進入之前被處理掉了。
果然，昨天愛米莉亞的暴走對阿沃斯・迪爾海維亞來說也是出乎預料的事情吧。
事情鬧大的話，計劃就會出問題。
雖然這樣下來對媽媽下手的可能性就沒那麼高了，不過也不可大意。
就算莎夏在我身邊，也一定會有所準備的吧。

「⋯⋯怎麼了？」

米莎盯著我的臉。

「不是什麼大事」
「有什麼我能做的嗎？」

明明說了不是什麼大事。

「好吧。那麼給我助威吧」

米莎歪著小腦袋。

「助威？」
「你說過要來助威的吧。我沒怎麼被做過這種事。」

米莎點了點頭。

「我明白了」

她邁著小碎步走到我身旁，拉起了我的手。
有一雙小手握住了那裡。

「不怕不怕」
「好吧，雖說本來就沒在害怕」

米莎像在想著什麼一樣低下頭去，過了一會又抬了起來。

「阿諾斯會贏」
「因為還沒有輸過」

米莎似乎有點困擾，又再次開始思考。

「我很開心阿諾斯能贏」
「我想即使暴虐的魔王獲得了冠軍，也沒有什麼有趣的啊？」

米歇爾搖了搖頭。

「阿諾斯是同學，是朋友」
「是啊」
「雷也是如此。同一班的兩人，要在決定迪爾海德第一劍豪的魔劍大會的決賽中戰鬥」

米莎用平常的平坦語調說。

「很了不起」
「是嗎？」

這時，室內響起了〈思念通信〉。

「讓您久等了。那麼，迪爾海德魔劍大會決賽現在開始！首先登場的是戴爾佐格德魔王學院所屬的阿諾斯・沃爾迪戈德選手！！」

看樣子時間到了。

「我去了」

朝著通向闘技場舞台的通道走去。
米莎對著我的背影說。

「阿諾斯重生了」

回頭一看，米莎直勾勾地凝視著我的眼睛。

「現在是學生」

米莎微笑著說。

「好好享受吧」

呼姆。感覺不錯。
心情不錯啊。這就是所謂的助威吧。
明知我是暴虐的魔王，米莎卻不顧曾經的我，而是看著現在的我。
重生的我。
乏味，無聊的學院生活。
太過弱小的子孫們，退化的魔法術式。
沒有任何東西可以學。
就算在這裡成就什麼，也沒有任何意義。
儘管如此，確實，想要這個。
這段平靜生活的時間。

「米莎」

她歪著頭。好像在問「什麼？」

我笑著說。

「我會奪冠的」
「嗯」

轉過身去，我徑直朝著通道走去。
走向朋友等待的決賽舞台。

────────────────────

米莎經常看著阿諾斯。
回來後，真的收到了很多感想，嚇了一跳。
非常感謝。我一個一個都好好地讀過了。
給了我很大的鼓勵。
感想回復昨晚還咯噔咯噔地做著，不過，稍微量有點多還不能全部回復完。
非常抱歉。另外，今天晚上，我也會繼續回復您的感想。
雖然每天的更新越來越嚴格，
我想以這樣的更新速度努力到自己的極限。
馬上就到二章的高潮階段了。
決賽會發生什麼呢？希望能讓您盡興。